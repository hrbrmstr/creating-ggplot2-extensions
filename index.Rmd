--- 
title: "Creating ggplot2 Extensions"
author: "Bob Rudis"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib]
biblio-style: apalike
link-citations: yes
github-repo: hrbrmstr/creating-ggplot2-extensions
url: 'https\://rud.is/books/creating-ggplot2-extensions'
description: "A learn-by-example guide to building new Geoms, Stats, Coords and more to extend and customize the functionality of ggplot2."
---

# Preface {-}

If you're reading this tome you are likely familiar with the [`ggplot2`](http://ggplot2.tidyverse.org/) package and have used it to create one or more data-driven visualizations.

The goal of this book is to help you move from being a _user_ of `ggplot2` into a creator of new `ggplot2` components that others can use. There are many types of components you can build for `ggplot`, including those involving:

- statistical computation (`Stat`s in `ggplot2` nomenclature)
- new visual elements (`Geom`s)
- coordinate systems (`Coord`s) 
- data transformation (scales)
- overall plot appearance (themes)

You will progress from developing basic components in standalone scripts/projects to packaging up your creations to make them easier to distribute so others can more readily use them.