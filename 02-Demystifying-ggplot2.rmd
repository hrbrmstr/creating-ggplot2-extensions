```{r include=FALSE}
knitr::opts_chunk$set(dpi=144)
```
# Demystifying `ggplot2`

The `ggplot2` system is elegant and expressive&hellip;once you finally wrap your head around it. For many, there's a steep learning curve to `ggplot2` and that learning curve often creates an aire of mysticism around what exactly goes on behind the scenes that ends up producing the magical creations that are `ggplot2` visualizations.

Now, there's an [entire book by Hadley](https://github.com/hadley/ggplot2-book) on `ggplot2` and scads of other books written by others on `ggplot2`. This chapter is not going to cover `ggplot2` in the same way. Rather, the goal, here, is to give you a sense of what goes on at a lower-level when you create a plot to help illumniate what you'll be doing when you start building `Geom`s, `Stat`s and other core `ggplot2` objects. 

## Breaking down the seminal example

There is a classic (seminal) example plot which budding `ggplot2` enthusiasts meet as their first foray into the grammar of graphics and that most regular users of `ggplot2` can produce at-will from memory:

```{r}
library(ggplot2)
```
```{r figure_02_01, cache=TRUE}

ggplot(mpg, aes(displ, hwy, colour = class)) + 
  geom_point()
```

As a `ggplot2` user, you know that:

- a data frame was passed in
- `x` and `y` aesthetics were mapped to specific data frame columns
- there is an intent to color whatever shape is being used by the contents of the `class` column
- the desired shape to use is a point.

But, what does that code _really do_?

Before delving into that, though, you may not even be aware that `ggplot2` filled in a bunch of missing information for you. Here's (for the most part) what was done for you:

```{r figure_02_02, eval=FALSE}
ggplot(mpg, aes(displ, hwy, colour = class)) +
  geom_point(stat = "identity", position = "identity", shape = 19, size = 1.5) +
  scale_x_continuous(trans = "identity") +
  scale_y_continuous(trans = "identity") +
  scale_color_hue() +
  coord_cartesian() +
  facet_null() +
  theme_gray()
```

After analyzing the input data and aesthetic mappings, `ggplot2` is able to "automagically" determine whether to use discrete or continuous scales for various mapped values and sets a number of other critical components from sensible, thoughtful defaults. This "magic" helps reduce typing and enables you to focus on customizing only what is absolutely necessary to convey the story you're trying to tell with the visualization.

There is one missing line from that code sequence: `print()`. 

By default, R prints evaluated objects and all you've done before printing is create a small, fairly complex `ggplot`-classed object with good intentions.

The `ggplot2::print.ggplot2()` function takes these intentions and transorms them --- with the aid of `ggplot_build()` and `ggplot_gtable()` --- into larger and even more complex structures, which are ultimately transformed into (hopefully) pretty pictures. 

Examining these objects will help you get a feel for what you'll ultimately be doing inside your own customized `ggplot2` object.

## The `ggplot` object

The first object to explore is the `ggplot` object itself. To that end, assign a plot to a variable and examine the structure with `str()`

```{r}
ggplot(mpg, aes(displ, hwy, colour = class)) + 
  geom_point() -> gg

str(gg)
```

Yikes! Perhaps it would be better to examine that in a _bit_ more of a deliberate fashion.

There are 9 elements in the `list` that make up the `ggplot`-classed object:

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements.png")`
</center>

The `data` element is what was passed in as `data` to `ggplot()`:

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-data.png")`
</center>

The `layers` element is a length 1 `list` of `ggproto` objects (which are the building blocks you'll be eventually creating). There is quite a bit of (for now) extraneous internal `ggproto` object information cluttering up the structure display, but it can be `print()`ed more compactly:

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-layers.png")`
</center>

There is one layer with a `Point` `Geom`, an `Identity` `Stat` and an `Identity` `Position`. Make a mental note of that as it will become a familiar idiom if you get into the habit of making customized `ggplot2` objects.

The `scales` element is a `ScalesList` object (see `scales-.r` in the `ggplot2` source) which _would_ contain scales you manually added to the `ggplot` object. Since the `gg` object is based on the minimal, seminal example, the defaults haven't been computed yet (that's `ggplot_build()`, coming up soon) so the length is `0` which can be verified with:

```{r}
gg$scales$n()
```

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-scaleslist.png")`
</center>

The `mapping` contains the aesthetic mappings that were created by one or more of the `aes()` family of functions. `x`, `y` and `colour` (note the spelling of that last one) all map to the expected data frame columns:

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-mapping.png")`
</center>

The `theme` element is also empty since an explicit `theme` has not been specified. When a theme is specified, the `list` structure will contain the details of _all_ the various theme element settings (can can become quite long).

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-theme.png")`
</center>

Unlike some of the other unspecified elements, the `coordinates` element does have a default value of `CoordCartesian` object:

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-coord.png")`
</center>

Since no faceting was specified, the default "null" facet is added to the plot in the `facet` element: 

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-facet.png")`
</center>

Penultimately, `ggplot2` stores the environemnt where it will pick up plot values from (in this case, the global environemnt):

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-env.png")`
</center>

And, finally, `ggplot2` shows off how smart it is by providing a list of the `labels` it managed to figure out from the mapped aesthetics:

<center>
`r knitr::include_graphics("figures/02-ggplot2-object-list-elements-labels.png")`
</center>

Believe it or not --- after all that --- you're really not much closer to having a visualization in front of you. A large chunk of the real work happens in `ggplot_build()`.

## The `ggplot_built` object

To examine a `ggplot_buit` object, it first needs to be created:

```{r eval=TRUE}
gb <- ggplot_build(gg)

```
```{r eval=FALSE}
str(gb)
## List of 3
##  $ data  :List of 1
##  $ layout:Classes 'Layout', 'ggproto', 'gg' <ggproto object: Class Layout, gg>
##  $ plot  :List of 9
##   ..- attr(*, "class")= chr [1:2] "gg" "ggplot"
##  - attr(*, "class")= chr "ggplot_built"
```

Astute readers who are typing along at home will notice that the display is more compact than the actual `str()` since you've seen a number of the structures already in the previous verbose display. There are key differences that will be covered.

First up is the `plot` element. This contains a copy of data from the `gg` object itself but a few of the missing pieces have been filled in. In particular, the `gb$plot$scales` now has three `Scale` objects:

- `<ScaleContinuousPosition>`
- `<ScaleContinuousPosition>`
- `<ggproto object: Class ScaleDiscrete, Scale, gg>`

which align with the `x`, `y` and `colour` column values that were passed in. 

Now, the `gb$plot$data` element is still there and is the same as the `gg$data` element. However, there's a new `data` element at the top level of `gb` and it's a `list`, which suggests that it could --- in other situations --- contain more than one element. In this case there is one element and it is a data frame, but it is noticeably different than the one in `gb$plot$data` (NOTE: `tibble::as_tibble()` is being used to make the object display easier to read):

```{r}
tibble::as_tibble(gb$plot$data)
```

```{r}
tibble::as_tibble(gb$data[[1]])
```

The original data has been transformed:

- there is a new, computed `colour` column that contains hex colors generated from the default discrete color scale (hue)
- the original column names mapped to `x` and `y` are now just `x` and `y` 
- there is a new `PANEL` column which indicates which facet the associated data elements are to be drawn on (only one for this plot given the lack of facets)
- a `group` column has been added and computed based on the number of unique, discrete elements in `mpg$class`
- `shape`, `size`, `fill`, `alpha` and `stroke` have also been added and set with the defaults for the aesthetic maps and parameters for the specified layers.

_Remember this structure_. When you build `ggplot2` custom `Geoms` (and other objects) one big part of that is creating this structure (or, _these structures_ if more than one data frame is being mapped to various aesthetics and shapes).

The `layout` element of the `gb` object is just a more detailed/complete/computed version of what was passed in from the `gg` object (more detail will be provided on that once the underlying structure of `Geom`s, `Stats`, etc are covered).

If you just enter `gb` at a console prompt you will see a plot due the `ggplot`-classed `plot` list element. Despite some transformations and data additions the plot is not yet ready for display. That's the job of `ggplot_gtable()`.

## The `gtable` object

The details of the `gtable` object will be reviewed in a later chapter, but you do need to know a bit more about the object, now, before moving on to making your first `Geom`/`Stat`. 

```{r echo=FALSE}
options(width=120)
```
```{r}
gt <- ggplot_gtable(gb)

print(gt)
```

The final object in the journey from grammar of graphics to final visualization is a `grid` graphics `gtable` object which is a structured representation of `grob`s --- `gr`aphical `ob`jects --- that contains everything necessary for the `grid` graphics system to transfer your visualization intent to an R graphics device. 

For now, the most important thing to notice is that each top-level `grob` has:

- a `z` rendering order
- `t`op, `r`ight, `b`ottom, `l`eft position extents in `cells`
- a `name` (which is very important as you'll see later)
- the `grob` itself (which could be -- and likely is -- a table or list of other `grob`s)

To prove this is the final step, just do:

```{r}
library(grid)

grid.newpage()
grid.draw(gt)
```

## Exercises

Before moving on, you should get cozy with the `ggplot` and `ggplot_built` structures. Cozy enough that you should be able to read the output of `str` on them from other `ggplot2` creations and be able to read them without _too much_ reliance on the `ggplot2` source code (using the source code as a reference is totally okay, though since the important part is familiarity and not wrote memorization).

To that end, try the following exercises:

- Incrementally build upon the initial, tiny example at the beginning of this chapter, changing and adding aesthetics, geoms, coordinates, themes, etc and examine the `gg` and `gb` structures after each one. See how they morph and grow. Don't skimp on this part! You will get a better understanding of what you're manipulating if you see how the standard/traditional `ggplot2` operations work.
- Create or find a complex `ggplot2` example that incorporates multiple data sources and fine-grained customization to see just how complex these objects can get and where various transformations take place. 
- For each of the above, look at the created `gtable` and note any top-level differences. That introspection will come in handy later.
- For each reference to a `ggproto` object in any `str()` output you create, make a "map" of which `ggplot2` source code file it is in. This will be an invaluable guide for you as you continue on this gg-journey.